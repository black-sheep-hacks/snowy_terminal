#!/usr/bin/env python3

import os
import random
import signal
import time


class Snow:
    _width = 0
    _height = 0
    _lines = []
    def __init__(self):
        self._set_size_and_clear()
    
    def _set_size_and_clear(self):
        height, width = os.popen('stty size', 'r').read().split()
        self._height, self._width = int(height), int(width)
        self._clear()
    
    def _clear(self):
        """
        Clear terminal and lines buffer.
        """
        print('\033[0;0H', end='')
        for line in range(self._height):
            print(' ' * self._width)
        self._lines = []

    def _add_line(self):
        line = [
            '\033[1;37m*' if random.randint(0, 5) == 0 else ' '
            for _ in range(self._width)
        ]
        self._lines.insert(0, line)
        self._lines = self._lines[:self._height+1]
    
    def _maybe_swing_flakes(self, line: list) -> list:
        for position in range(len(line)):
            if '*' in line[position]:
                left_char = line[position-1] if position > 1 else None
                right_char = line[position+1] if position < len(line) -1 else None
                if random.randint(0, 5) == 0 and line[position] != left_char and left_char:
                    line[position-1], line[position] = line[position], line[position-1]
                if random.randint(0, 5) == 0 and line[position] != right_char and right_char:
                    line[position+1], line[position] = line[position], line[position+1]
                    
        return line

    def resize_terminal(self, *args):
        self._set_size_and_clear()
    
    def __call__(self):
        while True:
            print('\033[0;0H', end='')
            self._add_line()
            for line in self._lines[:-2]:
                print(''.join(self._maybe_swing_flakes(line)))
            print(''.join(self._lines[-1]), end='\r')
            time.sleep(0.7)


if __name__ == '__main__':
    try:
        snow = Snow()
        signal.signal(signal.SIGWINCH, snow.resize_terminal)
        snow()
    except KeyboardInterrupt:
        print('Merry Christmas 🎅🎄🎁')
